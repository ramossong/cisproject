class CreateDiagcodes < ActiveRecord::Migration
  def change
    create_table :diagcodes do |t|
      t.string :dcode
      t.string :name

      t.timestamps
    end
  end
end
