class CreateAppointments < ActiveRecord::Migration
  def change
    create_table :appointments do |t|
      t.integer :physician_id
      t.integer :patient_id
      t.datetime :datetime
      t.decimal :copay

      t.timestamps
    end
  end
end
