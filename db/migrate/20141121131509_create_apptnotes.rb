class CreateApptnotes < ActiveRecord::Migration
  def change
    create_table :apptnotes do |t|
      t.integer :appointment_id
      t.integer :diagcode_id
      t.integer :invoice_id
      t.string :notes

      t.timestamps
    end
  end
end
