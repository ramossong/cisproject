class CreateInvoices < ActiveRecord::Migration
  def change
    create_table :invoices do |t|
      t.integer :patient_id
      t.integer :apptnote_id

      t.timestamps
    end
  end
end
