class CreatePatients < ActiveRecord::Migration
  def change
    create_table :patients do |t|
      t.string :fname
      t.string :lname
      t.string :address
      t.string :phone

      t.timestamps
    end
  end
end
