class CreatePhysicians < ActiveRecord::Migration
  def change
    create_table :physicians do |t|
      t.string :fname
      t.string :lname

      t.timestamps
    end
  end
end
