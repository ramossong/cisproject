# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
require 'open-uri'
require 'active_record/fixtures'

open("lib/assets/patients_seed.txt") do |patients|
  patients.read.each_line do |patient|
    fname, lname, address, phone = patient.chomp.split(",")
    Patient.create!(:fname => fname, :lname => lname, :address => address, :phone => phone)
  end
end

open("lib/assets/physicians_seed.txt") do |physicians|
  physicians.read.each_line do |physician|
    fname, lname = physician.chomp.split(",")
    Physician.create!(:fname => fname, :lname => lname)
  end
end

open("lib/assets/diagcodes_seed.txt") do |diagcodes|
  diagcodes.read.each_line do |diagcode|
    dcode, name = diagcode.chomp.split("|")
    Diagcode.create!(:dcode => dcode, :name => name)
  end
end