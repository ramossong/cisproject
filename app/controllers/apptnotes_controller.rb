class ApptnotesController < ApplicationController
  before_action :set_apptnote, only: [:show, :edit, :update, :destroy]

  # GET /apptnotes
  # GET /apptnotes.json
  def index
    @apptnotes = Apptnote.all
  end

  # GET /apptnotes/1
  # GET /apptnotes/1.json
  def show
  end

  # GET /apptnotes/new
  def new
    @apptnote = Apptnote.new
  end

  # GET /apptnotes/1/edit
  def edit
  end

  # POST /apptnotes
  # POST /apptnotes.json
  def create
    @apptnote = Apptnote.new(apptnote_params)

    respond_to do |format|
      if @apptnote.save
        format.html { redirect_to @apptnote, notice: 'Apptnote was successfully created.' }
        format.json { render :show, status: :created, location: @apptnote }
      else
        format.html { render :new }
        format.json { render json: @apptnote.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /apptnotes/1
  # PATCH/PUT /apptnotes/1.json
  def update
    respond_to do |format|
      if @apptnote.update(apptnote_params)
        format.html { redirect_to @apptnote, notice: 'Apptnote was successfully updated.' }
        format.json { render :show, status: :ok, location: @apptnote }
      else
        format.html { render :edit }
        format.json { render json: @apptnote.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /apptnotes/1
  # DELETE /apptnotes/1.json
  def destroy
    @apptnote.destroy
    respond_to do |format|
      format.html { redirect_to apptnotes_url, notice: 'Apptnote was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_apptnote
      @apptnote = Apptnote.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def apptnote_params
      params.require(:apptnote).permit(:appointment_id, :diagcode_id, :invoice_id, :notes)
    end
end
