class Appointment < ActiveRecord::Base
  has_many :apptnotes
  belongs_to :physician
  belongs_to :patient

  def patient_appointment
    self.includes(:appointments).where(:pat_id=>Patient.find(patient_id))
  end

  def physician_appointment
    self.includes(:appointments).where(:phy_id=>Physician.find(physician_id))
  end

end
