json.array!(@patients) do |patient|
  json.extract! patient, :id, :fname, :lname, :address, :phone
  json.url patient_url(patient, format: :json)
end
