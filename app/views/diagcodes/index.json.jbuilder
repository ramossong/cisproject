json.array!(@diagcodes) do |diagcode|
  json.extract! diagcode, :id, :dcode, :name
  json.url diagcode_url(diagcode, format: :json)
end
