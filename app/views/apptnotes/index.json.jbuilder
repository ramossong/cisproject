json.array!(@apptnotes) do |apptnote|
  json.extract! apptnote, :id, :appointment_id, :diagcode_id, :invoice_id, :notes
  json.url apptnote_url(apptnote, format: :json)
end
