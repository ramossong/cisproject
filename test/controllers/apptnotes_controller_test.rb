require 'test_helper'

class ApptnotesControllerTest < ActionController::TestCase
  setup do
    @apptnote = apptnotes(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:apptnotes)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create apptnote" do
    assert_difference('Apptnote.count') do
      post :create, apptnote: { appointment_id: @apptnote.appointment_id, diagcode_id: @apptnote.diagcode_id, invoice_id: @apptnote.invoice_id, notes: @apptnote.notes }
    end

    assert_redirected_to apptnote_path(assigns(:apptnote))
  end

  test "should show apptnote" do
    get :show, id: @apptnote
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @apptnote
    assert_response :success
  end

  test "should update apptnote" do
    patch :update, id: @apptnote, apptnote: { appointment_id: @apptnote.appointment_id, diagcode_id: @apptnote.diagcode_id, invoice_id: @apptnote.invoice_id, notes: @apptnote.notes }
    assert_redirected_to apptnote_path(assigns(:apptnote))
  end

  test "should destroy apptnote" do
    assert_difference('Apptnote.count', -1) do
      delete :destroy, id: @apptnote
    end

    assert_redirected_to apptnotes_path
  end
end
