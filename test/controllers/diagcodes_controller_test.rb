require 'test_helper'

class DiagcodesControllerTest < ActionController::TestCase
  setup do
    @diagcode = diagcodes(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:diagcodes)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create diagcode" do
    assert_difference('Diagcode.count') do
      post :create, diagcode: { dcode: @diagcode.dcode, name: @diagcode.name }
    end

    assert_redirected_to diagcode_path(assigns(:diagcode))
  end

  test "should show diagcode" do
    get :show, id: @diagcode
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @diagcode
    assert_response :success
  end

  test "should update diagcode" do
    patch :update, id: @diagcode, diagcode: { dcode: @diagcode.dcode, name: @diagcode.name }
    assert_redirected_to diagcode_path(assigns(:diagcode))
  end

  test "should destroy diagcode" do
    assert_difference('Diagcode.count', -1) do
      delete :destroy, id: @diagcode
    end

    assert_redirected_to diagcodes_path
  end
end
